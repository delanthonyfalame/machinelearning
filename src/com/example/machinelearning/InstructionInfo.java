package com.example.machinelearning;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.machinelearning.Adapter.SearchAdapter;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.DictionaryHandler;
import com.example.machinelearning.Database.InstructionHandler;

public class InstructionInfo extends SwitchFragmentParent{
	protected static final String TAG = "Instruction Info";
	private View view;
	ArrayList<DictionaryHandler> dictionary = new ArrayList<DictionaryHandler>();
	private int instructionID;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.instruction_item, container, false);
		db = new Database(getActivity());
		Bundle bundle = this.getArguments();
		instructionID = bundle.getInt("instruction");
		setWidgets();
		return view;
	}

	private void setWidgets() {
		for(InstructionHandler instruction : db.getAllInstruction())
			Log.i(TAG, "dictionary id : " + instruction.getBase_id()+ " == "  + instructionID);
		
		//get data from database base on id
		InstructionHandler term = db.getInstruction(instructionID);
		
		TextView title = (TextView) view.findViewById(R.id.title);
		TextView header = (TextView) view.findViewById(R.id.header);
		TextView content = (TextView) view.findViewById(R.id.content);
		
		title.setText(term.getTitle());
		header.setText(term.getHeader());
		content.setText(term.getContent());
		
	}
}
