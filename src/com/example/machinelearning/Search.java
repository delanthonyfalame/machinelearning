package com.example.machinelearning;

import java.util.ArrayList;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.machinelearning.Adapter.SearchAdapter;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.DictionaryHandler;

public class Search extends SwitchFragmentParent{
	protected static final String TAG = "Search";
	private View view;
	ArrayList<DictionaryHandler> dictionary = new ArrayList<DictionaryHandler>();
	ArrayList<DictionaryHandler> dictionaryTemp = new ArrayList<DictionaryHandler>();
	SearchAdapter adapter;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.search, container, false);
		db = new Database(getActivity());
		setWidgets();
		return view;
	}

	private void setWidgets() {
		
		ListView list = (ListView) view.findViewById(R.id.list);
		dictionary = db.getAllDictionary();
		dictionaryTemp = db.getAllDictionary();
		adapter = new SearchAdapter(getActivity(), dictionary);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				Log.i(TAG , dictionary.get(position).getMeaning());
				switchFrag.switchSearch(dictionary.get(position).getBase_id(), new SearchInfo());
			}
		});
		
		EditText search = (EditText) view.findViewById(R.id.search);
		search.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2, int count) {
				dictionary.clear();
				for (int i = 0; i < dictionaryTemp.size(); i++) {
					if(count <= dictionaryTemp.get(i).getName().length()){
						if(dictionaryTemp.get(i).getName().toLowerCase().contains(cs.toString().toLowerCase())){
							dictionary.add(dictionaryTemp.get(i));
						}
					}
				}
				adapter.notifyDataSetChanged();
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}
