package com.example.machinelearning.Database;

public class Choices {
	int base_id, id, chapter, lesson;
	String answer, dummy1, dummy2, dummy3, date_entry;
	public Choices(int id, int base_id, String date_entry, int chapter, int lesson, String answer, String dummy1, String dummy2, String dummy3) {
		this.id = id;
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.chapter = chapter;
		this.lesson = lesson;
		this.answer = answer;
		this.dummy1 = dummy1;
		this.dummy2 = dummy2;
		this.dummy3 = dummy3;
	}
	
	public Choices( int base_id, String date_entry, int chapter, int lesson, String answer, String dummy1, String dummy2, String dummy3) {
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.chapter = chapter;
		this.lesson = lesson;
		this.answer = answer;
		this.dummy1 = dummy1;
		this.dummy2 = dummy2;
		this.dummy3 = dummy3;
	}

	public int getBase_id() {
		return base_id;
	}

	public void setBase_id(int base_id) {
		this.base_id = base_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getChapter() {
		return chapter;
	}

	public void setChapter(int chapter) {
		this.chapter = chapter;
	}

	public int getLesson() {
		return lesson;
	}

	public void setLesson(int lesson) {
		this.lesson = lesson;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getDummy1() {
		return dummy1;
	}

	public void setDummy1(String dummy1) {
		this.dummy1 = dummy1;
	}

	public String getDummy2() {
		return dummy2;
	}

	public void setDummy2(String dummy2) {
		this.dummy2 = dummy2;
	}

	public String getDummy3() {
		return dummy3;
	}

	public void setDummy3(String dummy3) {
		this.dummy3 = dummy3;
	}

	public String getDate_entry() {
		return date_entry;
	}

	public void setDate_entry(String date_entry) {
		this.date_entry = date_entry;
	}
	
	
	
}
