package com.example.machinelearning.Database;

public class MarkHandler {
	int question_id, quiz;
	
	public MarkHandler(int question_id, int quiz) {
		this.question_id = question_id;
		this.quiz = quiz;
	}

	public int getQuestion_id() {
		return question_id;
	}

	public void setQuestion_id(int question_id) {
		this.question_id = question_id;
	}

	public int getQuiz() {
		return quiz;
	}

	public void setQuiz(int quiz) {
		this.quiz = quiz;
	}
	
	
	
}
