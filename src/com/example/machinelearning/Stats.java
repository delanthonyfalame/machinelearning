package com.example.machinelearning;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.machinelearning.Adapter.QuizResultAdapter;
import com.example.machinelearning.Adapter.StatisticsAdapter;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.QuizTotal;
import com.example.machinelearning.Database.Quiz_resultHandler;
import com.example.machinelearning.Database.StatisticsHandler;

public class Stats extends SwitchFragmentParent{
	private View view;
	ArrayList<StatisticsHandler> statistics;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		db = new Database(getActivity());
		view = inflater.inflate(R.layout.statistics, container, false);
		setWidgets();
		return view;
	}

	private void setWidgets() {
		statistics = db.getAllStatisticsGroupByID();
		ListView list = (ListView) view.findViewById(R.id.list);
		list.setAdapter(new StatisticsAdapter(getActivity(), statistics));
	}

}
