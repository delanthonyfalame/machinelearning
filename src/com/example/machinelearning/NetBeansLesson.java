package com.example.machinelearning;

import java.util.ArrayList;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.LessonHandler;
import com.example.machinelearning.Database.QuizHandler;
import com.example.machinelearning.imagedownloader.ImageLoader;

public class NetBeansLesson extends SwitchFragmentParent{

	protected static final String TAG = "Java Lesson";
	int chapterNumber;
	int lessonNumber;
	LessonHandler lesson;
	int maxLesson = 0;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.lesson, container, false);
		imageLoad  = new ImageLoader(getActivity());
		Bundle bundle = this.getArguments();
		lessonNumber = bundle.getInt("lesson");
		chapterNumber = bundle.getInt("chapter");
		Log.i(TAG, "lesson number " +  lessonNumber + " chapter " + chapterNumber);
		db = new Database(getActivity());
		lesson = db.getNetBeansIDandChapter(lessonNumber, chapterNumber);
		maxLesson = db.getMaxValueNetBeans(chapterNumber);
		Log.i(TAG, " chapter "+ chapterNumber + " lesson " + lessonNumber);
		setWidgets();
		return view; 
	}

	private void setWidgets() {
		//initialize all variables
		TextView title = (TextView) view.findViewById(R.id.title);
		TextView content = (TextView) view.findViewById(R.id.content);
		ImageView image = (ImageView) view.findViewById(R.id.image);
		imageLoad.DisplayImage(lesson.getImage(), image, R.drawable.ic_launcher);
		image.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showFullScreenImage(lesson.getImage());
				
			}
		});
		//fill xml
		title.setText(lesson.getTitle());
		content.setText(Html.fromHtml(lesson.getContent()));
		//set click listener on back button
		TextView back = (TextView) view.findViewById(R.id.back);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "back");
				switchFrag.goBack(new Java());
			}
		});
		
		
		//set click listener on quiz button
		TextView quiz = (TextView) view.findViewById(R.id.quiz);
		//hide quiz
		if(lessonNumber == maxLesson){
			quiz.setVisibility(View.VISIBLE);
		}else
			quiz.setVisibility(View.GONE);
		quiz.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG, "back");
				ArrayList<QuizHandler> quizArray = db.getNetBeansAllQuizByChapter(chapterNumber);
				if(quizArray != null)
					switchLesson.switchToQuiz(chapterNumber, new NetBeansQuiz());
				else
					Toast.makeText(getActivity(), "No quiz found in this chapter", Toast.LENGTH_SHORT).show();
			}
		});
		//set click listener on next button
		TextView next = (TextView) view.findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				int nextLesson  = db.getNetBeansNextLesson(lessonNumber, chapterNumber);
				Log.i(TAG, "next lesson : " + nextLesson + " previous " + lessonNumber + "chapter " + chapterNumber);
				LessonHandler lesson = db.getNetBeansIDandChapter(nextLesson, chapterNumber);
				if(lesson!=null)
					switchFrag.nextLesson(nextLesson, chapterNumber , new NetBeansLesson());
				else
					switchFrag.goBack(new NetBeansLesson());
			}
		});
	}
	

	protected void showFullScreenImage(String string) {
		final Dialog dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
		dialog.setContentView(R.layout.image_view);
		ImageView image = (ImageView) dialog.findViewById(R.id.image);
		
		imageLoad.DisplayImage(string, image, R.drawable.ic_launcher);
		
		dialog.show();
	}

}