package com.example.machinelearning;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.machinelearning.Adapter.InstructionAdapter;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.InstructionHandler;

public class Instructions extends SwitchFragmentParent{
	private View view;
	ArrayList<InstructionHandler> instructions  = new ArrayList<InstructionHandler>();
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		db = new Database(getActivity());
		view = inflater.inflate(R.layout.instruction, container, false);
		setWidgets();
		return view;
	}

	private void setWidgets() {
		ListView list = (ListView) view.findViewById(R.id.list);
		instructions = db.getAllInstruction();
		list.setAdapter(new InstructionAdapter(getActivity(), instructions));
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				switchFrag.switchInstruction(instructions.get(position).getBase_id(), new InstructionInfo());
			}
		});
	}

}
