package com.example.machinelearning;

import java.text.SimpleDateFormat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.example.machinelearning.SwitchFragmentParent.SwitchFragment;
import com.example.machinelearning.SwitchFragmentParent.SwitchToLesson;
import com.example.machinelearning.Database.Database;
import com.example.machinelearning.Database.Simulation_quizHandler;

/**
 * @author user
 *
 */
public class MainActivity extends SherlockFragmentActivity implements SwitchFragment, SwitchToLesson {

	private static final String TAG = "Main Fragment";
	Database db = new Database(this);
	/* (non-Javadoc)
	 * initialize all variables
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		addSimulation();

		if(savedInstanceState == null){
			// add quiz to database
			FragmentManager fm = getSupportFragmentManager();
			FragmentTransaction transaction = fm.beginTransaction();
			//replace fragments
			transaction.replace(R.id.fragment, new Loading()).addToBackStack(null);
			//confirm changes
			transaction.commit();
		}else
			switchFragment(new Dashboard());
	}


	@Override
	public void onBackPressed() {
		FragmentManager fm = getSupportFragmentManager();
		if(fm.getBackStackEntryCount() > 2)
			super.onBackPressed();
		else
			return;
	}


	/* (non-Javadoc)
	 * switch layout... implemented using interface.. in SwitchFragmentParent.java
	 * @see com.example.machinelearning.SwitchFragmentParent.SwitchFragment#switchFragment(android.support.v4.app.Fragment)
	 */
	@Override
	public void switchFragment(Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		//replace fragments
		transaction.replace(R.id.fragment, fragment);
		//add to back stack
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commitAllowingStateLoss();
	}

	Bundle bundle;
	@Override
	public void switchToLesson(int chapter, Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();

		FragmentTransaction transaction = fm.beginTransaction();
		//replace fragments
		transaction.replace(R.id.fragment, fragment);
		//add to back stack
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commit();
	}

	@Override
	public void switchToQuiz(int chapter, Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		//replace fragmentss
		transaction.replace(R.id.fragment, fragment);
		// initialize bundle
		bundle = new Bundle();
		//add data
		bundle.putInt("chapter", chapter);
		//add data to fragment
		fragment.setArguments(bundle);
		//lesson
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		transaction.commit();

	}


	@Override
	public void nextLesson(int lesson, int chapter, Fragment fragment) {
		Log.i(TAG, "lesson " + lesson);
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		// initialize bundle
		bundle = new Bundle();
		//add data
		bundle.putInt("lesson", lesson);
		bundle.putInt("chapter", chapter);
		//add data to fragment
		fragment.setArguments(bundle);
		//replace fragmentss
		transaction.replace(R.id.fragment, fragment);

		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		//lesson
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commit();
	}


	@Override
	public void goBack(Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		getSupportFragmentManager().popBackStackImmediate(backStateName, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		//		onBackPressed();
	}


	@Override
	public void switchSearch(int id, Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		// initialize bundle
		bundle = new Bundle();
		//add data
		bundle.putInt("dictionary", id);
		//add data to fragment
		fragment.setArguments(bundle);
		//replace fragmentss
		transaction.replace(R.id.fragment, fragment);

		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		//lesson
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commit();
	}


	@Override
	public void switchInstruction(int instructionID, Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		// initialize bundle
		bundle = new Bundle();
		//add data
		bundle.putInt("instruction", instructionID);
		//add data to fragment
		fragment.setArguments(bundle);
		//replace fragmentss
		transaction.replace(R.id.fragment, fragment);

		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		//lesson
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commit();

	}

	//	@Override
	//	public void playVideo(String location) {
	//		FragmentManager fm = getSupportFragmentManager();
	//		FragmentTransaction transaction = fm.beginTransaction();
	//		// initialize bundle
	//		bundle = new Bundle();
	//		//add data
	//		Log.i(TAG, "location " + location);
	//		bundle.putString("url", location);
	//		//set Fragment
	//		Fragment fragment = new Video();
	//		//add data to fragment
	//		fragment.setArguments(bundle);
	//		//replace fragmentss
	//		transaction.replace(R.id.fragment, fragment);
	//		String backStateName = fragment.getClass().getName();
	//		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
	//		//lesson
	//		transaction.addToBackStack(backStateName);
	//		//confirm changes
	//		transaction.commit();
	//	}


	public String getTimeStomp() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(new java.util.Date());
	}


	@Override
	public void switchFragment(Fragment fragment, Bundle bundle) {
		String backStateName = fragment.getClass().getName();
		FragmentManager fm = getSupportFragmentManager();
		//set arguemtns to fragment
		fragment.setArguments(bundle);
		//begin transaction
		FragmentTransaction transaction = fm.beginTransaction();
		//replace fragments
		transaction.replace(R.id.fragment, fragment);
		//add to back stack
		transaction.addToBackStack(backStateName);
		//confirm changes
		transaction.commit();
	}

	
	private void addSimulation() {
		db.addSimulation_quiz(new Simulation_quizHandler(1, getTimeStomp(), "My\nJava\nNetBeans\nTutor\nFirstProgram", "What is the output of the program that is provided below:\n\npublic class MyFirstJavaProgram{\n\n\tpublic static void main(String []args){\n\t\tSystem.out.println(\"My\");\n\t\tSystem.out.println(\"Java\");\n\t\tSystem.out.println(\"NetBeans\");\n\t\tSystem.out.println(\"Tutor\");\n\t\tSystem.out.print(\"First\");\n\t\tSystem.out.print(\"Program\");\n\t}\n}"));
		db.addSimulation_quiz(new Simulation_quizHandler(2, getTimeStomp(), "name  : Ransika\nsalary :1000.0", "Provided below is an Instance variables sample program\nprovide the output of the sample program below:\n\nimport a.io.*;\n\npublic class Employee{\n\n\t\tpublic String name;\n\t\tprivate double salary;\n\t\t\tpublic Employee (String empName){\n\t\t\t\tname = empName;\n\t\t\t}\n\n\t\t\tpublic void setSalary(double empSal){\n\t\t\t\tsalary = empSal;\n\t\t\t}\n\n\t\t\tpublic void printEmp(){\n\t\t\t\tSystem.out.println(\"name  : \" + name );\n\t\t\t\tSystem.out.println(\"salary :\" + salary);\n\t\t\t}\n\n\t\t\tpublic static void main(String args[]){\n\t\t\t\tEmployee empOne = new Employee(\"Ransika\");\n\t\t\t\tempOne.setSalary(1000);\n\t\t\t\tempOne.printEmp();\n\t\t}\n}"));
		db.addSimulation_quiz(new Simulation_quizHandler(3, getTimeStomp(), "Value of b is : 30\nValue of b is : 20", "Provided below is a Conditional Operator sample program.\nprovide the output of the sample program below:\n\npublic class Test{\n\n\tpublic static void main(String args[]){\n\t\tint a , b;\n\t\ta = 10;\n\t\tb = (a == 1) ? 20: 30;\n\t\tSystem.out.println(\"Value of b is :\" +  b );\n\n\t\tb = (a == 10) ? 20: 30;\n\t\tSystem.out.println(\"Value of b is :\" + b );\n\t}\n}"));
		db.addSimulation_quiz(new Simulation_quizHandler(4, getTimeStomp(), "Passed Name is :tommy\nPuppy's age is :2\nVariable Value :2", "Provided below is a sample program using objects, method ans instance variable.\nprovide the output of the sample program below:\npublic class Puppy{\n\n\tint puppyAge;\n\n\tpublic Puppy(String name){\n\tSystem.out.println(\"Passed Name is :\" + name );\n\t}\n\tpublic void setAge( int age ){\n\t\tpuppyAge = age;\n\t}\n\n\tpublic int getAge( ){\n\t\tSystem.out.println(\"Puppy's age is :\" + puppyAge );\n\t\treturn puppyAge;\n\t}\n\tpublic static void main(String []args){\n\t\tPuppy myPuppy = new Puppy( \"tommy\" );\n\t\tmyPuppy.setAge( 2 );\n\t\tmyPuppy.getAge( );\n\t\tSystem.out.println(\"Variable Value :\" + myPuppy.puppyAge );\n\t}\n}"));
		db.addSimulation_quiz(new Simulation_quizHandler(5, getTimeStomp(), "Well done\nYour grade is a C", "Provided below is a program using the switch decision making statement.\nprovide the output of the program below:\npublic class Test{\n\n\tpublic static void main(String args[]){\n\t\tchar grade = \'C\';\n\n\t\t\tswitch(grade)\n\t\t\t{\n\t\t\t\tcase \'A\':\n\t\t\t\t\tSystem.out.println(\"Excellent!\");\n\t\t\t\t\tbreak;\n\t\t\t\tcase \'B\' :\n\t\t\t\tcase \'C\' :\n\t\t\t\t\tSystem.out.println(\"Well done\");\n\t\t\t\t\tbreak;\n\t\t\t\tcase \'D\' :\n\t\t\t\t\tSystem.out.println(\"You passed\");\n\t\t\t\tcase \'F\' :\n\t\t\t\t\tSystem.out.println(\"Better try again\");\n\t\t\t\t\tbreak;\n\t\t\t\tdefault :\n\t\t\t\t\tSystem.out.println(\"Invalid grade\");\n\t\t\t}\n\t\t\tSystem.out.println(\"Your grade is \" + grade);\n\t\t}\n}"));
		db.addSimulation_quiz(new Simulation_quizHandler(6, getTimeStomp(), "10,20,30,40,50,\nJames,Larry,Tom,Lacy,", "Provided below is a program using the for loop statement.\nprovide the output of the program below:\n\npublic class Test{\n\n\tpublic static void main(String args[]){\n\t\tint [] numbers = {10, 20, 30, 40, 50};\n\n\t\tfor(int x : numbers ){\n\t\t\tSystem.out.print( x );\n\t\t\tSystem.out.print(\",\");\n\t\t}\n\t\tSystem.out.print(\"\\n\");\n\t\tString [] names ={\"James\", \"Larry\", \"Tom\", \"Lacy\"};\n\t\tfor( String name : names ) {\n\t\t\tSystem.out.print( name );\n\t\t\tSystem.out.print(\",\");\n\t\t}\n\t}\n}"));
		db.addSimulation_quiz(new Simulation_quizHandler(7, getTimeStomp(), "value of x : 10\nvalue of x : 11\nvalue of x : 12\nvalue of x : 13\nvalue of x : 14\nvalue of x : 15\nvalue of x : 16\nvalue of x : 17\nvalue of x : 18\nvalue of x : 19", "Provided below is a program using the for While loop statement.\nprovide the output of the program below:\npublic class Test {\n\n\tpublic static void main(String args[]) {\n\t\tint x = 10;\n\n\t\twhile( x < 20 ) {\n\t\t\tSystem.out.print(\"value of x : \" + x );\n\t\t\tx++;\n\t\t\tSystem.out.print(\"\\n\");\n\t\t}\n\t}\n}"));
		db.addSimulation_quiz(new Simulation_quizHandler(8, getTimeStomp(), "Sample : 5\nSample : 4\nSample : 3\nSample : 2\nSample : 1\nSample : 0", "Provided below is a program using the for do While loop statement.\nprovide the output of the program below:\n\nclass DoWhile {\n\tpublic static void main(String args[]) {\n\t\tint n = 5;\n\t\tdo\n\t\t\t{\n\t\t\tSystem.out.println(\"Sample : \" + n);\n\t\t\tn--;\n\t\t}while(n > 0);\n}"));
		db.addSimulation_quiz(new Simulation_quizHandler(9, getTimeStomp(), "1.9\n2.9\n3.4\n3.5", "Provided below is a program using Array.\nprovide the output of the program below:\npublic class TestArray {\n\n\tpublic static void main(String[] args) {\n\t\tdouble[] myList = {1.9, 2.9, 3.4, 3.5};\n\n\t\t// Print all the array elements\n\t\tfor (double element: myList) {\n\t\t\tSystem.out.println(element);\n\t\t}\n\t}\n}"));
	
	
	}

}
