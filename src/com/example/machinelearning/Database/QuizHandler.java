package com.example.machinelearning.Database;

public class QuizHandler {
	int id, base_id, chapter, lesson, mark;
	String date_entry, answer, dummy1, dummy2, dummy3, question, title, hint, image;
	
	public QuizHandler() {
		// TODO Auto-generated constructor stub
	}
	public QuizHandler(int id, int base_id, String date_entry, int lesson, int chapter, int mark, 
			String answer, String dummy1, String dummy2, String dummy3, String question, String title, String hint, String image) {
		this.id = id;
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.lesson = lesson;
		this.chapter = chapter;
		this.mark = mark;
		this.answer = answer;
		this.dummy1 = dummy1;
		this.dummy2 = dummy2;
		this.dummy3 = dummy3;
		this.question = question;
		this.title = title;
		this.hint = hint;
		this.image = image;
	}
	
	
	public QuizHandler( int base_id, String date_entry, int lesson, int chapter, int mark, 
			String answer, String dummy1, String dummy2, String dummy3, String question, String title, String hint, String image) {
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.lesson = lesson;
		this.chapter = chapter;
		this.mark = mark;
		this.answer = answer;
		this.dummy1 = dummy1;
		this.dummy2 = dummy2;
		this.dummy3 = dummy3;
		this.question = question;
		this.title = title;
		this.hint = hint;
		this.image = image;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getBase_id() {
		return base_id;
	}


	public void setBase_id(int base_id) {
		this.base_id = base_id;
	}


	public int getChapter() {
		return chapter;
	}


	public void setChapter(int chapter) {
		this.chapter = chapter;
	}


	public int getLesson() {
		return lesson;
	}


	public void setLesson(int lesson) {
		this.lesson = lesson;
	}


	public int getMark() {
		return mark;
	}


	public void setMark(int mark) {
		this.mark = mark;
	}


	public String getDate_entry() {
		return date_entry;
	}


	public void setDate_entry(String date_entry) {
		this.date_entry = date_entry;
	}


	public String getAnswer() {
		return answer;
	}


	public void setAnswer(String answer) {
		this.answer = answer;
	}


	public String getDummy1() {
		return dummy1;
	}


	public void setDummy1(String dummy1) {
		this.dummy1 = dummy1;
	}

	public String getDummy2() {
		return dummy2;
	}


	public void setDummy2(String dummy2) {
		this.dummy2 = dummy2;
	}


	public String getDummy3() {
		return dummy3;
	}


	public void setDummy3(String dummy3) {
		this.dummy3 = dummy3;
	}


	public String getQuestion() {
		return question;
	}


	public void setQuestion(String question) {
		this.question = question;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getHint() {
		return hint;
	}


	public void setHint(String hint) {
		this.hint = hint;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}
	
}
