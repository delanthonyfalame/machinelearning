package com.example.machinelearning.Database;
public class DictionaryHandler{
	private final static String TAG = "Dictionary";
	int id; 
	int base_id; 
	String date_entry; 
	String meaning; 
	String syllables; 
	String name; 

	/**======================= Default Constructor ========================**/

	public DictionaryHandler(){

	}

	public DictionaryHandler(int id, int base_id, String date_entry, String meaning, String syllables, String name){
		this.id = id;
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.meaning = meaning;
		this.syllables = syllables;
		this.name = name;
	}

	public DictionaryHandler( int base_id, String date_entry, String meaning, String syllables, String name){
		this.base_id = base_id;
		this.date_entry = date_entry;
		this.meaning = meaning;
		this.syllables = syllables;
		this.name = name;
	}

	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id = id;
	}

	public int getBase_id(){
		return base_id;
	}
	public void setBase_id(int base_id){
		this.base_id = base_id;
	}

	public String getDate_entry(){
		return date_entry;
	}
	public void setDate_entry(String date_entry){
		this.date_entry = date_entry;
	}

	public String getMeaning(){
		return meaning;
	}
	public void setMeaning(String meaning){
		this.meaning = meaning;
	}

	public String getSyllables(){
		return syllables;
	}
	public void setSyllables(String syllables){
		this.syllables = syllables;
	}

	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
}